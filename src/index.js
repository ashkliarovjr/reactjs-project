import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';


const SubmitBtn = ({ className }) => (
    <button id="submit" type="submit" className={`btn submit-btn ${className}`}>
        Submit
    </button>
);

function Email(props) {
    return (
        <input autoComplete="false" type="email" className="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" onChange={props.onChange}/>
    )
}

function Password(props) {
    return (
        <input autoComplete="false" type="password" className="form-control" id="password" placeholder="Password" onChange={props.onChange}/>
    )
}

class Form extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isEmailValid: false,
            isPassValid: false
        }
    }

    handleEmailChange = (event) => {
        const email = event.target.value;
        this.setState({isEmailValid : email && email.length > 0 && email.includes('@')})
    };

    handlePasswordChange = (event) => {
        const pass = event.target.value;
        const validationRegex = new RegExp('^[a-zA-Z]+$')
        this.setState({isPassValid : validationRegex.test(pass)})
    };

    getSubmitColorClass() {
        return (this.state.isEmailValid && this.state.isPassValid) ? 'btn-success' : 'btn-secondary'
    }

    renderSubmitBtn() {
        return <SubmitBtn className={this.getSubmitColorClass()}/>
    }

    renderEmailField() {
        return <Email onChange={(event) => this.handleEmailChange(event)}/>
    }

    renderPassField() {
        return <Password onChange={(event) => this.handlePasswordChange(event)}/>
    }

    render() {
        return (
            <form autoComplete="off">
                <div className="form-group">
                    <label htmlFor="email-lbl">Email address</label>
                    {this.renderEmailField()}
                </div>
                <div className="form-group">
                    <label htmlFor="pass-lbl">Password</label>
                    {this.renderPassField()}
                </div>
                {this.renderSubmitBtn()}
            </form>
        );
    }
}

ReactDOM.render(
    <Form/>,
    document.getElementById('root')
);

